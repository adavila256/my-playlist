import React, {useEffect, useState} from 'react';
import './App.scss'
import SearchBar from "./components/SearchBar";
import {Container} from "reactstrap";
import Playlist from "./components/Playlist";
import VideoGrid from "./components/VideoGrid";

function App() {
    const [isGapiLoaded, setGapiState] = useState(false);
    const [videoList, setVideoList] = useState([]);
    const [playList, setPlayList] = useState([]);
    const [videoId, setVideoId] = useState("");

    useEffect(() => {
        const start = () => {
            window.gapi.client.init({
                'apiKey': 'AIzaSyD276h8KZOoL3nWStsM7Ai3x8ZyOUqnd3k',
                'discoveryDocs': ['https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest'],
            }).then(() => setGapiState(true))
        }
        window.gapi.load('client', start);
    }, [])

    const onDragOver = (ev) => {
        ev.preventDefault();
    }

    const drag = (ev) => {
        ev.dataTransfer.setData("text", ev.target.id);
    }

    const drop = (ev) => {
        ev.preventDefault();
        const data = ev.dataTransfer.getData("text");
        setPlayList(prev => [...prev, data])
    }

    const playVideo = (item) => {
        setVideoId(item.id.videoId)
    }

    const playVideoFromList = (item) => {
        setPlayList(playList.filter(i => i !== item.id.videoId))
        playVideo(item);
    }

    return (
        <div>
            <SearchBar className="my-3" enabled={isGapiLoaded} setVideoList={setVideoList}/>
            <Container fluid className="d-flex">
                <VideoGrid items={videoList} draggable={true} drag={drag} videoId={videoId} playVideo={playVideo}/>
                {videoId !== "" &&
                <Playlist items={playList} videoList={videoList} drop={drop} onDragOver={onDragOver}
                          playVideo={playVideoFromList}/>}
            </Container>
        </div>
    );
}

export default App;
