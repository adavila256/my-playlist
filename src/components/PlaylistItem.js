import React from "react";
import {Media} from "reactstrap";

const PlaylistItem = ({item, playVideo}) => {
    const {snippet} = item;
    const title = snippet.title;
    const thumbnail = snippet.thumbnails.medium;

    return (
        <div className="d-flex mb-2" onClick={() => playVideo(item)}>
            <Media className="img-thumbnail p-0 mr-1 pointer" width={"170"} object src={thumbnail.url} alt="vid-thumb"/>
            <div className="fnt-14px font-weight-bold">{title}</div>
        </div>
    )
}

export default PlaylistItem;