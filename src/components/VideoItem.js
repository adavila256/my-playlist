import React, {useState} from "react";
import {Media} from "reactstrap";
import classNames from "classnames"

const VideoItem = ({item, draggable, drag, playVideo}) => {
    const {snippet} = item;
    const title = snippet.title;
    const thumbnail = snippet.thumbnails.medium;
    const [isHover, setHover] = useState(false);

    const toggleIcon = () => {
        setHover(!isHover)
    }

    return (
        <div draggable={draggable} onDragStart={drag} id={item.id.videoId} onMouseEnter={toggleIcon}
             onClick={() => playVideo(item)}
             onMouseLeave={toggleIcon}
             className="max-w-300px position-relative mb-2">
            <Media draggable={false} className={"img-thumbnail p-0 mr-1 pointer"}
                   width={"100%"} object src={thumbnail.url} alt="vid-thumb"/>
            <div draggable={false} className="fnt-14px font-weight-bold">{title}</div>
            <i className={classNames("fa fa-play-circle-o icon-center fa-red fa-3x", {"opacity-1": isHover})}/>
        </div>
    )
}

export default VideoItem;