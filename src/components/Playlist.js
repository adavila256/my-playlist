import React from "react";
import PlaylistItem from "./PlaylistItem";

const Playlist = ({items, videoList, drop, onDragOver, playVideo}) => {

    return (
        <div className="playlist pl-2" onDrop={drop} onDragOver={onDragOver}>
            <div className="font-weight-bold fnt-14px">Playlist</div>
            {items.length === 0 &&
            <div className="fnt-12px fnt-gray">
                Tu lista de reproduccion aun esta vacia!! Busca y arrastra tus proximos videos...
            </div>}
            {
                items.map((id, idx) => {
                    let v = videoList.filter(vid => vid.id.videoId === id)
                    return (
                        <PlaylistItem key={idx} item={v[0]} playVideo={playVideo}/>
                    )
                })
            }
        </div>
    )
}

export default Playlist;