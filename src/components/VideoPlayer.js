import React from "react";

const VideoPlayer = ({videoId}) => {
    return (
        <div className="d-flex justify-content-center pb-2">
            <iframe id="player" type="text/html" width="640" height="390" frameBorder="0"
                    src={`http://www.youtube.com/embed/${videoId}?enablejsapi=1&origin=http://example.com`}
            />
        </div>
    )
}

export default VideoPlayer;