import React, {useRef} from "react";
import {Button, Input, InputGroup} from "reactstrap";

const SearchBar = ({className, enabled, setVideoList}) => {
    const inputRef = useRef();

    const executeSearch = () => {
        const currentSearch = inputRef.current.value;

        window.gapi.client.youtube.search.list({
            part: "snippet",
            maxResults: 50,
            type: 'video',
            q: currentSearch ? currentSearch : ""
        }).then(({result}) => {
            setVideoList(result.items)
        })
    }

    return (
        <div className={className}>
            <InputGroup className="max-w-500px mx-auto">
                <Input innerRef={inputRef} placeholder="Buscar" disabled={!enabled}/>
                <Button color="secondary" onClick={executeSearch} disabled={!enabled}>
                    <i className="fa fa-search"/>
                </Button>
            </InputGroup>
        </div>
    )
}

export default SearchBar;