import React from "react";
import VideoPlayer from "./VideoPlayer";
import VideoItem from "./VideoItem";

const VideoGrid = ({items, draggable, drag, playVideo, videoId}) => {
    return (
        <div className="px-2">
            {videoId !== "" && <VideoPlayer videoId={videoId}/>}
            <div className="d-flex flex-grow-1 flex-wrap justify-content-between">
                {
                    items.map((item, idx) => <VideoItem key={idx} draggable={draggable} item={item} drag={drag}
                                                        playVideo={playVideo}/>)
                }
            </div>
        </div>
    )
}

export default VideoGrid;